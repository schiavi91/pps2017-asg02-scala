package u04lab.solution

sealed trait SOption[A] {

  def isEmpty: Boolean

  def getOrElse[B >: A](orElse: B): B

  def filter(f: A => Boolean): SOption[A]

  def map[B](f: A => B): SOption[B]

  def map2[B, C](other: SOption[B])(f: (A,B) => C): SOption[C]
}

case class SNone[A]() extends SOptionImplementation[A]

case class SSome[A](_value: A) extends SOptionImplementation[A]

trait SOptionImplementation[A] extends SOption[A] {
  override def isEmpty: Boolean = this match {
    case SNone() => true
    case _       => false
  }

  override def getOrElse[B >: A](orElse: B): B = this match {
    case SSome(value) => value
    case _            => orElse
  }

  override def filter(f: A => Boolean): SOption[A] = this match {
    case SSome(value) if f(value) => SSome(value)
    case _                        => SNone()
  }

  def map[B](f: A => B): SOption[B] = this match {
    case SSome(value) => SSome(f(value))
    case _            => SNone()
  }

  override def map2[B, C](other: SOption[B])(f: (A,B) => C): SOption[C] = (this, other) match {
    case (SSome(value1), SSome(value2)) => SSome(f(value1, value2))
    case _ => SNone()
  }
}

object TestSOption extends App {
  val optSome = SSome(20)
  val optNone = SNone()
  println(optSome.isEmpty) // false
  println(optNone.isEmpty) // true
  println(optSome getOrElse "It is empty") // 20
  println(optNone getOrElse "It is empty") // It is empty
  println(optSome filter (_ > 10)) // SSome(20)
  println(optSome filter (_ < 10)) // SNone
  println(SNone[Int]() map (_ > 10)) // SNone
  println(optSome map (_ > 10)) // SSome(true)
  println(optSome map (_ < 10)) // SSome(false)
  println(SNone[Int]() map (_ > 10)) // SNone
  println(optSome.map2(SSome(5))(_ + _)) // SSome(25)
  println(optSome.map2(SSome("ciao"))(_ + _)) // SSome("20ciao")
  println(SSome("ciao").map2(optSome)(_ + _)) // SSome("ciao20")
  println(SNone[Int]().map2(optSome)(_ + _)) // SNone
  println(optSome.map2(SNone[Int]())(_ + _)) // SNone
}
