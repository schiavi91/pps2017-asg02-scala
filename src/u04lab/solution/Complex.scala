package u04lab.solution

// nel caso utilizzi una "case class" non ho bisogno di riscrivermi equals e toString

trait Complex {
  def re: Double
  def im: Double
  def +(c: Complex): Complex // should implement the sum of two complex numbers..
  def *(c: Complex): Complex // should implement the product of two complex numbers
  /*override def equals(obj: Any): Boolean
  override def toString: String*/
}

case class ComplexImpl( var re : Double,
                   var im : Double) extends Complex {
  def +(c: Complex): Complex = Complex(this.re + c.re,this.im + c.im)
  def *(c: Complex): Complex = Complex((this.re*c.re)-(this.im*c.im),(this.im*c.re)+(this.re*c.im))
  /* utilizzando unapply
  override def equals(obj: Any): Boolean = obj match {
    case that: Complex => (that.re == this.re) && (that.im == this.im)
    case _ => false
  }
  override def toString: String = {
    "ComplexImpl("+this.re+", "+this.im+")"
  }
  */
  /* senza utilizzare unapply
  override def equals(obj: Any): Boolean = {
    if(obj.isInstanceOf[Complex]){
      val complexObj= obj.asInstanceOf[Complex]
      if(this.re == complexObj.re && this.im == complexObj.im)
        true
      else
        false
    }
    else
      false
  }
  */
}

object Complex {
  def apply(re:Double, im:Double):Complex = ComplexImpl(re, im)
  /*
  def unapply(arg: Complex): Option[(Double, Double)] = Some((arg.re,arg.im));
   */
}

object TryComplex extends App {
  val a = Array(Complex(10,20), Complex(1,1), Complex(7,0), Complex(10,20))
  val c = a(0)+a(1)+a(2)
  println(c, c.re, c.im) // (ComplexImpl(18.0,21.0),18.0,21.0)
  val c2 = a(0)*a(1)
  println(c2, c2.re, c2.im) // (ComplexImpl(-10.0,30.0),-10.0,30.0)
  println(a(0).equals(a(2))) // false
  println(a(0).equals(a(3))) // true
}

/** Hints:
  * - implement Complex with a ComplexImpl class, similar to PersonImpl in slides
  * - check that equality and toString do not work
  * - use a case class ComplexImpl instead, creating objects without the 'new' keyword
  * - check equality and toString now
  */
